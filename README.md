# ��� ����� �� ���� �������������� � ��������� � ��� ������������ .terraform 
**/.terraform/*

# ��� ����� � ����������� tfstate, � ���������
*.tfstate

# ��� ����� � ����� �����������, ������������ � ������ ������ �������� �� �������� ������� .tfstate, � ���������
*.tfstate.*

# ���� crash.log
crash.log

# ��� ����� � ����������� tfvars, � ���������
*.tfvars

# ���� override.tf
override.tf

# ���� override.tf.json
override.tf.json

# ����� � ����������� tf, ������������ � ������ ������ �������� �� �������� ������� _override
*_override.tf

# ����� � ����������� json, ������������ � ������ ������ �������� �� �������� ������� _override.tf
*_override.tf.json

# ���� .terraformrc
.terraformrc

# ���� terraform.rc
terraform.rc